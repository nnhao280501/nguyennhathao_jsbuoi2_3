function tinhTongLuong() {

    //input: tien luong 1 ngay

    var tienLuong = document.getElementById('tienLuong1h').value;
    var soNgayLam = document.getElementById('soNgayLam').value;


    //output
    var tongLuong = 0;
    tongLuong = tienLuong * soNgayLam;

    document.getElementById('tongLuong').innerHTML = tongLuong;
}

// bài 2: tính giá trị trung bình 
function tinhTrungBinh() {

    // input nhập 5 số thực
    var soThu1 = document.getElementById('soThuc1').value;
    var soThu2 = document.getElementById('soThuc2').value;
    var soThu3 = document.getElementById('soThuc3').value;
    var soThu4 = document.getElementById('soThuc4').value;
    var soThu5 = document.getElementById('soThuc5').value;
    //output: tính trung bình
    var giaTriTB = 0;
    //progress
    giaTriTB = (+soThu1 + +soThu2 + +soThu3 + +soThu4 + +soThu5) / 5;
    // ra màn hình
    document.getElementById('giaTriTB').innerHTML = giaTriTB;

}



// bài 3: quy đổi tiền  
//input :nhập số tiền
function quyDoiTien() {
    var soTienVND = 23500;
    var soTienUSD = document.getElementById('tienUSD').value;
    //output: quy đổi tiền
    var quyDoi = 0;
    quyDoi = soTienUSD * soTienVND;
    // ra màn hình
    document.getElementById('quyDoi').innerHTML = quyDoi;
}




// bài 4: tính diện tích , chu vi hình chữ nhật 
// Input chiều dài ,chiều rộng
function tinhChuViVaDienTich() {
    var chieuDai = document.getElementById('chieuDaiHCN').value;
    var chieuRong = document.getElementById('chieuRongHCN').value;
    // Output tính chu vi và diện tích
    var chuVi = 0;
    var dienTich = 0;
    //progress
    chuVi = (+chieuDai + +chieuRong) * 2;
    dienTich = +chieuDai * +chieuRong;
    // ra màn hình
    document.getElementById('chuVi').innerHTML = chuVi;
    document.getElementById('dienTich').innerHTML = dienTich;

}





// bài 5: tính tổng 2 ký số
//Input: nhập số có 2 chữ số
function tinhTongHaiKySo() {
    var soNhap = document.getElementById('soCanNhap').value;
    var hangDV = 0;
    var hangChuc = 0;

    // output: tổng ký số
    var tongKySo = 0;
    // progress
    hangChuc = +soNhap / 10;
    hangDV = +soNhap % 10;
    tongKySo = +hangChuc + +hangDV;
    // ra màn hình
    document.getElementById('tongKySo').innerHTML = tongKySo;
}